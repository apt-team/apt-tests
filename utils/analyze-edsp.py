#!/usr/bin/python3
import apt_pkg
import argparse
import sys

parser = argparse.ArgumentParser()
parser.add_argument("scenario")
parser.add_argument("solution")
parser.add_argument("merged", nargs="?", type=argparse.FileType('w'))
args = parser.parse_args()

header = None
universe = {}
installed = {}

def fullname(sec):
    return sec["Package"] + ":" + (sec["Architecture"] if sec["Architecture"] != "all" else "amd64")

with apt_pkg.TagFile(args.scenario) as scf:
    header = dict(next(scf))
    for sec in scf:
        if  sec.get("Installed", "no") == "yes":
            installed[fullname(sec)] = sec["APT-ID"]
        universe[sec["APT-ID"]] = dict(sec)

after = dict(installed)
with apt_pkg.TagFile(args.solution) as scf:
    for sec in scf:
        ins = sec.get("Install")
        if ins:
            pkg = universe[ins]
            after[fullname(pkg)] = ins
        remove = sec.get("Remove")
        if remove:
            pkg = universe[remove]
            del after[fullname(pkg)]

upgrade = install = remove = remove_auto = 0
for pkg in after:
    if pkg in installed and after[pkg] != installed[pkg]:
        print("Upgrading", pkg, universe[installed[pkg]]["Version"], universe[after[pkg]]["Version"],  file=sys.stderr)
        upgrade += 1
for pkg in after:
    if pkg not in installed:
        print("Installing", pkg, file=sys.stderr)
        install += 1
for pkg in installed:
    auto = universe[installed[pkg]].get("APT-Automatic") == "yes"
    if auto and pkg not in after:
        print("Removing dependency", pkg, "auto", file=sys.stderr)
        remove_auto += 1
for pkg in installed:
    auto = universe[installed[pkg]].get("APT-Automatic") == "yes"
    if not auto and pkg not in after:
            print("Removing", pkg, file=sys.stderr)
            remove += 1

if args.merged:
    print("\n".join(": ".join(kv) for kv in header.items()), file=args.merged)
    print(file=args.merged)
    for id_ in universe:
        sec = universe[id_]
        if after.get(fullname(sec)) == sec["APT-ID"]:
            sec["Installed"] = "yes"
        elif "Installed" in sec:
            del sec["Installed"]
        print("\n".join(": ".join(kv) for kv in sec.items()), file=args.merged)
        print(file=args.merged)
print("%-20s Upgrading: %-3d | Installing %-3d | Removing depends: %-3d | Removing: %-3d    " % (
      args.solution, upgrade, install, remove_auto, remove))
