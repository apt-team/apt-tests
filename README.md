# Real-world APT test suite

This is a test suite consisting of real world dependency and installation
ordering problems in EDSP and EIPP format that were gathered from user bug
reports.

The intention is that this is pulled in by the apt unit test suite such that
we can test regressions of complex scenarios without having to stuff them all
into the source package.
